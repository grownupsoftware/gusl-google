package gusl.google;

import com.google.auth.oauth2.ServiceAccountCredentials;
import com.google.cloud.storage.Storage;
import com.google.cloud.storage.StorageOptions;
import gusl.google.assets.AssetBucket;
import lombok.CustomLog;
import org.junit.Test;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

/**
 * @author dhudson
 * @since 08/08/2022
 */
@CustomLog
public class MediaTest {

    @Test
    public void getMediaURL() throws IOException {

        try {

            File serviceFile = new File("/Users/dhudson/development/gusl/gusl-common-c7d4132ee78f.json");
            ServiceAccountCredentials credentials =
                    ServiceAccountCredentials.fromStream(new FileInputStream(serviceFile));

            StorageOptions options = StorageOptions.newBuilder()
                    .setCredentials(credentials)
                    .build();
//            String endpoint = "https://storage.googleapis.com";

//            Storage storage =
//                    StorageOptions.newBuilder().setProjectId("gusl-common").setHost(endpoint).build().getService();


//            Storage storage = StorageOptions.getDefaultInstance().getService();

            AssetBucket bucket = new AssetBucket(options, "images");

            logger.info("Media Link {}", bucket.getCloudFile("GeoIP2-City.mmdb").getMediaLink());

        } catch (IOException ex) {
            logger.info("Nope {}", ex);
        }
    }
}
