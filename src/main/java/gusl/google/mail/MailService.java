package gusl.google.mail;

import gusl.node.properties.GUSLConfigurable;
import org.jvnet.hk2.annotations.Contract;

/**
 * @author dhudson
 * @since 30/11/2021
 */
@Contract
public interface MailService extends GUSLConfigurable {

}
