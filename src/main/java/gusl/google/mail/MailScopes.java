package gusl.google.mail;

/**
 * @author dhudson
 * @since 01/12/2021
 */
public interface MailScopes {

    /** Read, compose, send, and permanently delete all your email from Gmail. */
    String MAIL_GOOGLE_COM = "https://mail.google.com/";

    /** Manage drafts and send emails when you interact with the add-on. */
    String GMAIL_ADDONS_CURRENT_ACTION_COMPOSE = "https://www.googleapis.com/auth/gmail.addons.current.action.compose";

    /** View your email messages when you interact with the add-on. */
    String GMAIL_ADDONS_CURRENT_MESSAGE_ACTION = "https://www.googleapis.com/auth/gmail.addons.current.message.action";

    /** View your email message metadata when the add-on is running. */
    String GMAIL_ADDONS_CURRENT_MESSAGE_METADATA = "https://www.googleapis.com/auth/gmail.addons.current.message.metadata";

    /** View your email messages when the add-on is running. */
    String GMAIL_ADDONS_CURRENT_MESSAGE_READONLY = "https://www.googleapis.com/auth/gmail.addons.current.message.readonly";

    /** Manage drafts and send emails. */
    String GMAIL_COMPOSE = "https://www.googleapis.com/auth/gmail.compose";

    /** Insert mail into your mailbox. */
    String GMAIL_INSERT = "https://www.googleapis.com/auth/gmail.insert";

    /** Manage mailbox labels. */
    String GMAIL_LABELS = "https://www.googleapis.com/auth/gmail.labels";

    /** View your email message metadata such as labels and headers, but not the email body. */
    String GMAIL_METADATA = "https://www.googleapis.com/auth/gmail.metadata";

    /** View and modify but not delete your email. */
    String GMAIL_MODIFY = "https://www.googleapis.com/auth/gmail.modify";

    /** View your email messages and settings. */
    String GMAIL_READONLY = "https://www.googleapis.com/auth/gmail.readonly";

    /** Send email on your behalf. */
    String GMAIL_SEND = "https://www.googleapis.com/auth/gmail.send";

    /** Manage your basic mail settings. */
    String GMAIL_SETTINGS_BASIC = "https://www.googleapis.com/auth/gmail.settings.basic";

    /** Manage your sensitive mail settings, including who can manage your mail. */
    String GMAIL_SETTINGS_SHARING = "https://www.googleapis.com/auth/gmail.settings.sharing";
}
