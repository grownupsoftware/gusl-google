package gusl.google.mail;

import com.google.auth.oauth2.AccessToken;
import gusl.core.exceptions.GUSLException;
import gusl.model.nodeconfig.NodeConfig;
import lombok.CustomLog;
import org.jvnet.hk2.annotations.Service;

/**
 * @author dhudson
 * @since 30/11/2021
 */
@Service
@CustomLog
public class MailServiceImpl implements MailService {

    private static final String SERVICE_ACCOUNT = "gmail-service-key.json";

    private static final int PORT = 587;
    private static final String HOST = "smtp.gmail.com";

    private AccessToken theToken;

    private final String FROM = "darren@supernovasports.com";
    private final String TO = "DazHudson@gmail.com";


    public void doStuff() {
//        Properties props = new Properties();
//        props.put("mail.smtp.starttls.enable", "true");
//        props.put("mail.smtp.starttls.required", "true");
//        props.put("mail.smtp.sasl.enable", "true");
//        props.put("mail.smtp.sasl.mechanisms", "XOAUTH2");
//        props.put(OAuth2SaslClientFactory.OAUTH_TOKEN_PROP, theToken.getTokenValue());
//
//        Session session = Session.getInstance(props);
//        session.setDebug(true);
//
//        try {
//            // Create a default MimeMessage object.
//            Message message = new MimeMessage(session);
//
//            // Set From: header field of the header.
//            message.setFrom(new InternetAddress(FROM));
//
//            // Set To: header field of the header.
//            message.setRecipients(Message.RecipientType.TO,
//                    InternetAddress.parse(TO));
//
//            // Set Subject: header field
//            message.setSubject("Testing Subject");
//
//            // Now set the actual message
//            message.setText("Hello, this is sample for to check send email using JavaMailAPI ");
//
//            // Send message
//            final URLName unusedUrlName = null;
//            SMTPTransport transport = new SMTPTransport(session, unusedUrlName);
//            // If the password is non-null, SMTP tries to do AUTH LOGIN.
//            final String emptyPassword = "";
//            transport.connect(HOST, PORT, "114960316132059959076", ""); //theToken.getTokenValue());
//            transport.send(message);
//
//            logger.info("Sent message successfully....");
//        } catch (Throwable t) {
//            logger.warn("Unable to send message due to ", t);
//        }

    }

    @Override
    public void configure(NodeConfig config) throws GUSLException {
//        try {
//            Security.addProvider(new OAuth2Provider());
//
//            // "https://www.googleapis.com/auth/gmail.send"
//            theToken = GoogleUtils.generateAccessToken(SERVICE_ACCOUNT, Collections.singleton("https://www.googleapis.com/auth/gmail.send"));
//
//            logger.info("Access Token {}", theToken);
//        } catch (IOException ex) {
//            logger.warn("Error creating Gmail Service", ex);
//        }
    }

}
