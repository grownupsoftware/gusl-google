package gusl.google.utils;

import com.google.auth.oauth2.AccessToken;
import com.google.auth.oauth2.GoogleCredentials;
import com.google.auth.oauth2.ServiceAccountCredentials;
import gusl.core.utils.IOUtils;

import java.io.IOException;
import java.io.InputStream;
import java.util.Collection;

/**
 * @author dhudson on 18/07/2020
 */
public class GoogleUtils {

    private GoogleUtils() {
    }

    public static ServiceAccountCredentials getServiceAccount(String jsonFile) throws IOException {
        return ServiceAccountCredentials.fromStream(serviceFileAsStream(jsonFile));
    }

    public static AccessToken generateAccessToken(String jsonFile, Collection<String> scopes) throws IOException {
        return GoogleCredentials.fromStream(serviceFileAsStream(jsonFile))
                .createScoped(scopes).refreshAccessToken();
    }

    private static InputStream serviceFileAsStream(String jsonFile) throws IOException {
        return IOUtils.getResourceAsStream(jsonFile, GoogleUtils.class.getClassLoader());
    }
}
