package gusl.google.sheets;

import com.google.api.services.sheets.v4.Sheets;
import com.google.api.services.sheets.v4.model.Spreadsheet;
import com.google.api.services.sheets.v4.model.SpreadsheetProperties;
import gusl.core.exceptions.GUSLException;
import gusl.model.nodeconfig.NodeConfig;
import lombok.CustomLog;
import org.jvnet.hk2.annotations.Service;

import java.io.IOException;

/**
 * https://developers.google.com/sheets/api/quickstart/java
 * https://www.baeldung.com/google-sheets-java-client
 * https://github.com/eugenp/tutorials/commit/18c8747c98d0c89c64e1a7db8df80413737a4ef7
 *
 * @author dhudson on 18/07/2020
 */
@Service
@CustomLog
public class SheetServiceImpl implements SheetService {

    private Sheets theService;

    @Override
    public Spreadsheet createSpreadSheet(String title) throws IOException {
        Spreadsheet spreadsheet = new Spreadsheet().setProperties(new SpreadsheetProperties().setTitle(title));
        return theService.spreadsheets().create(spreadsheet).execute();
    }

    @Override
    public void configure(NodeConfig config) throws GUSLException {

//        try {
//            theService = new Sheets.Builder(GoogleNetHttpTransport.newTrustedTransport(), JacksonFactory.getDefaultInstance(),
//                    SheetUtils.authorize()).setApplicationName(GoogleUtils.APPLICATION_NAME).build();
//
//        } catch (IOException | GeneralSecurityException ex) {
//            logger.warn("Unable to start sheets service", ex);
//            throw new GUSLException("Unable to start sheets service", ex);
//        }
    }
}
