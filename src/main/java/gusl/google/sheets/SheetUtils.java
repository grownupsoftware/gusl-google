package gusl.google.sheets;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.extensions.java6.auth.oauth2.AuthorizationCodeInstalledApp;
import com.google.api.client.extensions.jetty.auth.oauth2.LocalServerReceiver;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow;
import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.util.store.MemoryDataStoreFactory;
import com.google.api.services.sheets.v4.SheetsScopes;
import gusl.core.utils.IOUtils;
import lombok.CustomLog;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.security.GeneralSecurityException;
import java.util.Arrays;
import java.util.List;

/**
 * @author dhudson on 18/07/2020
 */
@CustomLog
public class SheetUtils {

    private static final String SERVICE_FILE = "sheet-credentials.json";

    private SheetUtils() {
    }

    public static Credential authorize() throws IOException, GeneralSecurityException {
        JacksonFactory factory = JacksonFactory.getDefaultInstance();
        NetHttpTransport transport = GoogleNetHttpTransport.newTrustedTransport();
        List<String> scopes = Arrays.asList(SheetsScopes.SPREADSHEETS);

        InputStream in = IOUtils.getResourceAsStream(SERVICE_FILE, SheetUtils.class.getClassLoader());
        GoogleClientSecrets clientSecrets = GoogleClientSecrets
                .load(factory, new InputStreamReader(in));

        logger.info("{} : {}", clientSecrets.getDetails().getClientId(), clientSecrets.getDetails().getClientSecret());

        GoogleAuthorizationCodeFlow flow = new GoogleAuthorizationCodeFlow
                .Builder(transport,
                factory,
                clientSecrets,
                scopes)
                .setDataStoreFactory(new MemoryDataStoreFactory())
                .setAccessType("offline")
                .build();

        return new AuthorizationCodeInstalledApp(flow, new LocalServerReceiver())
                .authorize("user");
    }
}
