package gusl.google.sheets;

import com.google.api.services.sheets.v4.model.Spreadsheet;
import gusl.node.properties.GUSLConfigurable;
import org.jvnet.hk2.annotations.Contract;

import java.io.IOException;

/**
 * @author dhudson on 18/07/2020
 */
@Contract
public interface SheetService extends GUSLConfigurable {

    Spreadsheet createSpreadSheet(String title) throws IOException;

}
