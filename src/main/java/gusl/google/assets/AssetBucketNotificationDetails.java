package gusl.google.assets;

import com.fasterxml.jackson.annotation.JsonFormat;
import gusl.core.tostring.ToString;

import java.time.LocalDateTime;
import java.util.Map;

/**
 * @author dhudson on 2019-08-08
 */
public class AssetBucketNotificationDetails {

    private String kind;
    private String id;
    private String selfLink;
    private String name;
    private String bucket;
    private Long generation;
    private Long metageneration;
    private String contentType;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSXXX")
    private LocalDateTime timeCreated;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSXXX")
    private LocalDateTime updated;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSXXX")
    private LocalDateTime timeDeleted;
    private Boolean temporaryHold;
    private Boolean eventBasedHold;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSXXX")
    private LocalDateTime retentionExpirationTime;
    private String storageClass;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSXXX")
    private LocalDateTime timeStorageClassUpdated;
    private Long size;
    private String md5Hash;
    private String mediaLink;
    private String contentEncoding;
    private String contentDisposition;
    private String contentLanguage;
    private String cacheControl;
    private Map<String, String> metadata;
    private String crc32c;
    private Integer componentCount;
    private String etag;

    public AssetBucketNotificationDetails() {
    }

    public String getKind() {
        return kind;
    }

    public void setKind(String kind) {
        this.kind = kind;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSelfLink() {
        return selfLink;
    }

    public void setSelfLink(String selfLink) {
        this.selfLink = selfLink;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBucket() {
        return bucket;
    }

    public void setBucket(String bucket) {
        this.bucket = bucket;
    }

    public Long getGeneration() {
        return generation;
    }

    public void setGeneration(Long generation) {
        this.generation = generation;
    }

    public Long getMetageneration() {
        return metageneration;
    }

    public void setMetageneration(Long metageneration) {
        this.metageneration = metageneration;
    }

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    public LocalDateTime getTimeCreated() {
        return timeCreated;
    }

    public void setTimeCreated(LocalDateTime timeCreated) {
        this.timeCreated = timeCreated;
    }

    public LocalDateTime getUpdated() {
        return updated;
    }

    public void setUpdated(LocalDateTime updated) {
        this.updated = updated;
    }

    public LocalDateTime getTimeDeleted() {
        return timeDeleted;
    }

    public void setTimeDeleted(LocalDateTime timeDeleted) {
        this.timeDeleted = timeDeleted;
    }

    public Boolean getTemporaryHold() {
        return temporaryHold;
    }

    public void setTemporaryHold(Boolean temporaryHold) {
        this.temporaryHold = temporaryHold;
    }

    public Boolean getEventBasedHold() {
        return eventBasedHold;
    }

    public void setEventBasedHold(Boolean eventBasedHold) {
        this.eventBasedHold = eventBasedHold;
    }

    public LocalDateTime getRetentionExpirationTime() {
        return retentionExpirationTime;
    }

    public void setRetentionExpirationTime(LocalDateTime retentionExpirationTime) {
        this.retentionExpirationTime = retentionExpirationTime;
    }

    public String getStorageClass() {
        return storageClass;
    }

    public void setStorageClass(String storageClass) {
        this.storageClass = storageClass;
    }

    public LocalDateTime getTimeStorageClassUpdated() {
        return timeStorageClassUpdated;
    }

    public void setTimeStorageClassUpdated(LocalDateTime timeStorageClassUpdated) {
        this.timeStorageClassUpdated = timeStorageClassUpdated;
    }

    public Long getSize() {
        return size;
    }

    public void setSize(Long size) {
        this.size = size;
    }

    public String getMd5Hash() {
        return md5Hash;
    }

    public void setMd5Hash(String md5Hash) {
        this.md5Hash = md5Hash;
    }

    public String getMediaLink() {
        return mediaLink;
    }

    public void setMediaLink(String mediaLink) {
        this.mediaLink = mediaLink;
    }

    public String getContentEncoding() {
        return contentEncoding;
    }

    public void setContentEncoding(String contentEncoding) {
        this.contentEncoding = contentEncoding;
    }

    public String getContentDisposition() {
        return contentDisposition;
    }

    public void setContentDisposition(String contentDisposition) {
        this.contentDisposition = contentDisposition;
    }

    public String getContentLanguage() {
        return contentLanguage;
    }

    public void setContentLanguage(String contentLanguage) {
        this.contentLanguage = contentLanguage;
    }

    public String getCacheControl() {
        return cacheControl;
    }

    public void setCacheControl(String cacheControl) {
        this.cacheControl = cacheControl;
    }

    public Map<String, String> getMetadata() {
        return metadata;
    }

    public void setMetadata(Map<String, String> metadata) {
        this.metadata = metadata;
    }

    public String getCrc32c() {
        return crc32c;
    }

    public void setCrc32c(String crc32c) {
        this.crc32c = crc32c;
    }

    public Integer getComponentCount() {
        return componentCount;
    }

    public void setComponentCount(Integer componentCount) {
        this.componentCount = componentCount;
    }

    public String getEtag() {
        return etag;
    }

    public void setEtag(String etag) {
        this.etag = etag;
    }

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
