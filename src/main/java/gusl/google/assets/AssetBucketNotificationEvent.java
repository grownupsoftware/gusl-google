package gusl.google.assets;

import gusl.core.tostring.ToString;

/**
 * @author dhudson on 2019-08-08
 */
public class AssetBucketNotificationEvent {

    private AssetNotificationAttributes attrs;
    private AssetBucketNotificationDetails details;

    public AssetBucketNotificationEvent() {
    }

    public AssetNotificationAttributes getAttrs() {
        return attrs;
    }

    public void setAttrs(AssetNotificationAttributes attrs) {
        this.attrs = attrs;
    }

    public AssetBucketNotificationDetails getDetails() {
        return details;
    }

    public void setDetails(AssetBucketNotificationDetails details) {
        this.details = details;
    }

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
