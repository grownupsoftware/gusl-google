package gusl.google.assets;

import com.google.cloud.storage.*;
import gusl.core.tostring.ToString;

import java.io.File;
import java.io.InputStream;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.file.Path;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import static gusl.google.assets.AssetBucket.IMAGE_CACHE_CONTROL;

/**
 * Wraps a GCP Blob.
 * <p>
 * https://cloud.google.com/storage/docs/access-control/signing-urls-with-helpers#code-samples
 *
 * @author dhudson on 2019-07-29
 */
public class CloudFile {

    private URL theURL;
    private Blob theBlob;

    private LocalDateTime theExpiry;
    private String theSingedCachedUrl;

    public CloudFile() {
    }

    public CloudFile(URL uRL, Blob blob) {
        this.theURL = uRL;
        this.theBlob = blob;
    }

    public File asFile() {
        return new File(theURL.getFile());
    }

    public boolean isDirectory() {
        return theBlob.isDirectory();
    }

    public URL getURL() {
        return theURL;
    }

    public void setURL(URL theURL) {
        this.theURL = theURL;
    }

    public Blob getBlob() {
        return theBlob;
    }

    public void setBlob(Blob theBlob) {
        this.theBlob = theBlob;
    }

    public String getName() {
        return theBlob.getName();
    }

    /**
     * If embedding the image, you will need this URL.
     *
     * @return
     */
    public String getMediaLink() {
        return theBlob.getMediaLink();
    }

    /**
     * Historically, Casanova is expecting, ./ prefix to images.
     * <p>
     * This then gets replaced with the google storage URL.
     * </p>
     *
     * @return localised version of the URL
     */
    public String getLocalisedLink() {
        return "./" + theBlob.getName();
    }

    public String getExtension() {
        if (getName().lastIndexOf(".") != -1 && getName().lastIndexOf(".") != 0) {
            return getName().substring(getName().lastIndexOf(".") + 1);
        }

        return null;
    }

    public boolean isImage() {
        String extension = getExtension();
        if (extension == null) {
            return false;
        }

        switch (extension.toLowerCase()) {
            case "png":
            case "svg":
            case "mp4":
            case "jpg":
                return true;

            default:
                return false;
        }
    }

    /**
     * Check the cache control value, and set it if not default.
     *
     * @return true if the cache value was changed.
     */
    public boolean checkCacheValues() {
        if (!IMAGE_CACHE_CONTROL.equals(getBlob().getCacheControl())) {
            getBlob().toBuilder().setCacheControl(IMAGE_CACHE_CONTROL).build().update();
            return true;
        }

        return false;
    }

    public InputStream getInputStream() {
        return Channels.newInputStream(getBlob().reader());
    }

    public void downloadTo(Path path) {
        getBlob().downloadTo(path);
    }

    public String getMD5() {
        return getBlob().getMd5ToHexString();
    }

    public String getCRC() {
        return getBlob().getCrc32cToHexString();
    }

    public long copyAs(String path) {
        CopyWriter copyWriter = getBlob().copyTo(getBlob().getBucket(), path);
        return copyWriter.getTotalBytesCopied();
    }

    public byte[] getContents() {
        return theBlob.getContent();
    }

    public Map<String, String> getMetadata() {
        return getBlob().getMetadata();
    }

    public String getMetadataAttribute(String key) {
        return getBlob().getMetadata().get(key);
    }

    public URL getSignedURL() {
        BlobInfo info = BlobInfo.newBuilder(BlobId.of(theBlob.getBucket(), theBlob.getName())).build();
        return theBlob.getStorage().signUrl(info, 1, TimeUnit.DAYS, Storage.SignUrlOption.withV4Signature());
    }

    private void resetSignedUrl() {
        theSingedCachedUrl = getSignedURL().toString();
        theExpiry = LocalDateTime.now().plusDays(1).minusMinutes(10);
    }

    public String getCachedSignedUrl() {
        if(theSingedCachedUrl == null) {
            resetSignedUrl();
        } else if(LocalDateTime.now().isAfter(theExpiry)) {
            resetSignedUrl();
        }

        return theSingedCachedUrl;
    }

    public LocalDateTime getDateCreated() {
        return LocalDateTime.ofInstant(Instant.ofEpochMilli(theBlob.getCreateTime()), ZoneId.systemDefault());
    }

    public LocalDateTime getDateUpdated() {
        return LocalDateTime.ofInstant(Instant.ofEpochMilli(theBlob.getUpdateTime()), ZoneId.systemDefault());
    }

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
