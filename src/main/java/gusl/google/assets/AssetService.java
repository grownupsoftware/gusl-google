package gusl.google.assets;

import gusl.node.properties.GUSLConfigurable;
import org.jvnet.hk2.annotations.Contract;

import java.util.List;

/**
 * @author dhudson on 2019-07-17
 */
@Contract
public interface AssetService extends GUSLConfigurable {

    String BINARY_CONTENT_TYPE = "application/octet-stream";
    String DIRECTORY_CONTENT_TYPE = "application/x-directory";
    String JSON_CONTENT_TYPE = "application/json";
    String CSV_CONTENT_TYPE = "text/csv";

    AssetBucket getImageBucket();

    AssetBucket getRepositoryBucket();

    CloudFile savePlayerDocument(String path, byte[] imageBytes);

    /**
     * Get an image from the Image bucket.
     * <p>
     * These images are cached for future use.
     *
     * </p>
     *
     * @param path in the form of ./assets/....
     * @return the cached cloud file or null if not found
     */
    CloudFile getImageIconFromCache(String path);

    CloudFile getVideoImage(String path);

    List<CloudFile> getVideos();

    List<CloudFile> getPlayerDocs(String playerId);
}
