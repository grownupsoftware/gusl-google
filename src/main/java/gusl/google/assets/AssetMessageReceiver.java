package gusl.google.assets;

import com.google.cloud.pubsub.v1.AckReplyConsumer;
import com.google.cloud.pubsub.v1.MessageReceiver;
import com.google.pubsub.v1.PubsubMessage;
import lombok.CustomLog;

/**
 * @author dhudson on 2019-08-08
 * <p>
 * https://cloud.google.com/pubsub/docs/quickstart-client-libraries
 */
@CustomLog
public class AssetMessageReceiver implements MessageReceiver {

    public AssetMessageReceiver() {
    }

    @Override
    public void receiveMessage(PubsubMessage message, AckReplyConsumer consumer) {

        logger.debug("I have message ...  {}:Attrs {}: Payload {}", message.getMessageId(), message.getAttributesMap(), message.getData().toStringUtf8());

        // Only once everything is processed, Ack
        consumer.ack();
    }
}
