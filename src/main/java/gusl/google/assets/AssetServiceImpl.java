package gusl.google.assets;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.api.gax.core.FixedCredentialsProvider;
import com.google.auth.oauth2.ServiceAccountCredentials;
import com.google.cloud.pubsub.v1.AckReplyConsumer;
import com.google.cloud.pubsub.v1.MessageReceiver;
import com.google.cloud.pubsub.v1.Subscriber;
import com.google.cloud.storage.StorageOptions;
import com.google.pubsub.v1.ProjectSubscriptionName;
import com.google.pubsub.v1.PubsubMessage;
import gusl.core.exceptions.GUSLException;
import gusl.core.json.JsonUtils;
import gusl.core.json.ObjectMapperFactory;
import gusl.core.utils.IOUtils;
import gusl.core.utils.StringUtils;
import gusl.model.nodeconfig.BucketConfig;
import gusl.model.nodeconfig.BucketsConfig;
import gusl.model.nodeconfig.NodeConfig;
import gusl.node.eventbus.NodeEventBus;
import lombok.CustomLog;
import org.jvnet.hk2.annotations.Service;

import javax.inject.Inject;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static gusl.core.json.ObjectMapperFactory.LOWER_CAMEL_CASE;

/**
 * @author dhudson on 2019-07-17
 */
@Service
@CustomLog
public class AssetServiceImpl implements AssetService, MessageReceiver {

    @Inject
    private NodeEventBus theEventBus;

    private AssetBucket theImageBucket;
    private AssetBucket theDocumentBucket;
    private AssetBucket theRepositoryBucket;

    private final ObjectMapper theObjectMapper;
   private final Map<String, CloudFile> theIconImageMap;

    public AssetServiceImpl() {
        theObjectMapper = ObjectMapperFactory.getObjectMapperWithNaming(LOWER_CAMEL_CASE);
        theIconImageMap = new HashMap<>();
    }

    @Override
    public void configure(NodeConfig config) throws GUSLException {
        logger.debug("NodeConfig: {}", JsonUtils.prettyPrint(config));
        configure(config.getBucketsConfig());
    }

    public void configure(BucketsConfig bucketsConfig) throws GUSLException {
        try {
            if (bucketsConfig == null) {
                logger.warn("No buckets defined");
                return;
            }

            logger.debug("Service file: {}", JsonUtils.prettyPrint(bucketsConfig));
            ServiceAccountCredentials credentials = ServiceAccountCredentials.fromStream(
                    IOUtils.getResourceAsStream(bucketsConfig.getServiceFile(), AssetService.class.getClassLoader()));

            logger.debug("ServiceAccountCredentials: {}", JsonUtils.prettyPrint(credentials));

            StorageOptions options = StorageOptions.newBuilder()
                    .setCredentials(credentials)
                    .build();

            for (BucketConfig bucketConfig : bucketsConfig.getBuckets()) {

                AssetBucket bucket = createBucket(options, bucketConfig);

                switch (bucket.getType()) {
                    case DOCUMENTATION:
                        theDocumentBucket = bucket;
                        break;
                    case IMAGE:
                        theImageBucket = bucket;
                        break;
                    case REPOSITORY:
                        theRepositoryBucket = bucket;
                        break;
                }

                if (bucketConfig.getSubscriber() != null) {

                    ProjectSubscriptionName subscriptionName = ProjectSubscriptionName.of(credentials.getProjectId(), bucketConfig.getSubscriber());
                    Subscriber subscriber;

                    try {
                        logger.debug("Starting a notification subscriber for {} : {}", bucket.getName(), bucketConfig.getSubscriber());
                        // create a subscriber bound to the asynchronous message receiver
                        subscriber =
                                Subscriber.newBuilder(subscriptionName, this)
                                        .setCredentialsProvider(FixedCredentialsProvider.create(credentials))
                                        .build();
                        subscriber.startAsync().awaitRunning();
                        bucket.setNotificationSubscriber(subscriber);

                        // Allow the subscriber to run indefinitely unless an unrecoverable error occurs.
                        //subscriber.awaitTerminated();
                    } catch (IllegalStateException e) {
                        logger.error("Subscriber unexpectedly stopped", e);
                    }
                }
            }

            logger.info("Buckets Image {} - Documentation {} - Repository {}", theImageBucket, theDocumentBucket, theRepositoryBucket);

        } catch (IOException ex) {
            logger.warn("Unable to load storage module", ex);
            throw new GUSLException("Unable to load storage module", ex);
        }
    }

    private AssetBucket createBucket(StorageOptions options, BucketConfig config) {
        AssetBucket bucket = new AssetBucket(options, config.getName());
        bucket.setType(config.getType());

        return bucket;
    }

    @Override
    public AssetBucket getImageBucket() {
        return theImageBucket;
    }

    @Override
    public AssetBucket getRepositoryBucket() {
        return theRepositoryBucket;
    }

    @Override
    public CloudFile savePlayerDocument(String path, byte[] imageBytes) {
        return getRepositoryBucket().createImageBlob(path, imageBytes);
    }

    @Override
    public CloudFile getImageIconFromCache(String path) {
        if(path == null) {
            return null;
        }

        if(path.startsWith("./")) {
            path = path.substring(2);
        }

        try {
            if (getImageBucket() == null) {
                return null;
            }
            if (StringUtils.isBlank(path)) {
                return null;
            }

            CloudFile imageFile = theIconImageMap.get(path);

            if (imageFile == null) {
                imageFile = getImageBucket().getCloudFile(path);
                if (imageFile == null) {
                    return null;
                }
                theIconImageMap.put(path, imageFile);
            }

            return imageFile;
        } catch (Throwable t) {
            logger.error("Failed to get image [{}]", path, t);
            return null;
        }
    }

    @Override
    public void receiveMessage(PubsubMessage message, AckReplyConsumer consumer) {
        // No matter what, consume the message
        consumer.ack();

        try {
            AssetBucketNotificationEvent event = new AssetBucketNotificationEvent();
            event.setAttrs(new AssetNotificationAttributes(message.getAttributesMap()));
            event.setDetails(theObjectMapper.readValue(message.getData().toStringUtf8(), AssetBucketNotificationDetails.class));

            logger.info("Event ...  {}:{}", event.getAttrs().getBucketId(), event.getAttrs().getObjectId());

            if (theEventBus != null) {
                // Post the message
                theEventBus.post(event);
            }

        } catch (final IOException ex) {
            logger.warn("Unable to parse message {}", message, ex);
        }
    }

    @Override
    public CloudFile getVideoImage(String path) {
        return getRepositoryBucket().getCloudFile("backoffice/videos/" + path);
    }

    @Override
    public List<CloudFile> getVideos() {
        return getRepositoryBucket().getImageFilesFrom("backoffice/videos");
    }

    @Override
    public List<CloudFile> getPlayerDocs(String playerId) {
        return getRepositoryBucket().getImageFilesFrom("kyc/" + playerId);
    }
}
