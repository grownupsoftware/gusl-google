package gusl.google.assets;

import gusl.core.tostring.ToString;

import java.util.Map;

/**
 * @author dhudson on 2019-08-08
 */
public class AssetNotificationAttributes {

    private static final String BUCKET_ID = "bucketId";
    private static final String NOTIFICATION_CONFIG = "notificationConfig";
    private static final String OBJECT_ID = "objectId";
    private static final String OBJECT_GENERATION = "objectGeneration";
    private static final String EVENT_TIME = "eventTime";
    private static final String EVENT_TYPE = "eventType";

    // Sent when a new object (or a new generation of an existing object) is successfully created in the bucket.
    // This includes copying or rewriting an existing object. A failed upload does not trigger this event.
    public static final String OBJECT_FINALIZE_EVENT_TYPE = "OBJECT_FINALIZE";
    // Sent when the metadata of an existing object changes.
    public static final String OBJECT_METADATA_UPDATE_EVENT_TYPE = "OBJECT_METADATA_UPDATE";
    // Sent when an object has been permanently deleted. This includes objects that are overwritten or are deleted as
    // part of the bucket's lifecycle configuration. For buckets with object versioning enabled, this is not sent when
    // an object is archived (see OBJECT_ARCHIVE), even if archival occurs via the storage.objects.delete method.
    public static final String OBJECT_DELETE_EVENT_TYPE = "OBJECT_DELETE";
    // Only sent when a bucket has enabled object versioning. This event indicates that the live version of an object
    // has become an archived version, either because it was archived or because it was overwritten by the upload of an
    // object of the same name.
    public static final String OBJECT_ARCHIVE_EVENT_TYPE = "OBJECT_ARCHIVE";

    private final Map<String, String> theAttrs;

    public AssetNotificationAttributes(Map<String, String> attrs) {
        theAttrs = attrs;
    }

    public String getEventType() {
        return theAttrs.get(EVENT_TYPE);
    }

    public String getBucketId() {
        return theAttrs.get(BUCKET_ID);
    }

    public String getEventTime() {
        return theAttrs.get(EVENT_TIME);
    }

    public String getObjectGeneration() {
        return theAttrs.get(OBJECT_GENERATION);
    }

    public String getNotificationConfig() {
        return theAttrs.get(NOTIFICATION_CONFIG);
    }

    public String getObjectId() {
        return theAttrs.get(OBJECT_ID);
    }

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
