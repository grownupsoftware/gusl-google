package gusl.google.assets;

import com.google.api.gax.paging.Page;
import com.google.cloud.pubsub.v1.Subscriber;
import com.google.cloud.storage.*;
import com.google.common.io.Files;
import gusl.core.tostring.ToString;
import gusl.core.tostring.ToStringIgnore;
import gusl.core.utils.StringUtils;
import gusl.model.nodeconfig.BucketType;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static gusl.google.assets.AssetService.*;

// https://github.com/googleapis/google-cloud-java/blob/master/google-cloud-examples/src/main/java/com/google/cloud/examples/storage/snippets/StorageSnippets.java

// https://cloud.google.com/storage/docs/configuring-cors

/**
 * @author dhudson on 2019-07-22
 */
public class AssetBucket {

    public static final String URL_PREFIX = "https://storage.googleapis.com/";
    public static final String IMAGE_CACHE_CONTROL = "public, max-age=31536000";

    private final String name;
    private BucketType type;
    @ToStringIgnore
    private final StorageOptions theStorageOptions;
    private final String theEndpointUrl;
    @ToStringIgnore
    private Storage theStorage;

    private Subscriber theNotificationSubscriber;

    public AssetBucket(StorageOptions options, String bucketName) {
        theStorageOptions = options;
        theStorage = options.getService();
        name = bucketName;
        theEndpointUrl = URL_PREFIX + getName() + "/";
    }

    public String getName() {
        return name;
    }

    public BucketType getType() {
        return type;
    }

    public void setType(BucketType type) {
        this.type = type;
    }

    // Don't expose these, use cloud files instead
    Page<Blob> getBucketItems() {
        return theStorageOptions.getService().list(getName());
    }

    Page<Blob> getBucketItems(String directory) {
        return theStorageOptions.getService().list(getName(),
                Storage.BlobListOption.prefix(directory));
    }

    public List<CloudFile> getFiles() {
        return listStorage(false);
    }

    public List<CloudFile> getFilesAndDirectories() {
        return listStorage(true);
    }

    public List<CloudFile> getFilesFrom(String directory) {
        List<CloudFile> files = new ArrayList<>();
        for (Blob blob : getBucketItems(directory).iterateAll()) {
            if (!isDirectory(blob)) {
                files.add(createCloudFileFromBlob(blob));
            }
        }

        return files;
    }

    public List<CloudFile> getImageFiles() {
        return getFiles().stream().filter(file -> file.isImage()).collect(Collectors.toList());
    }

    public List<CloudFile> getImageFilesFrom(String directory) {
        return getFilesFrom(directory).stream().filter(file -> file.isImage()).collect(Collectors.toList());
    }

    // Oddly, is directory doesn't always work
    private boolean isDirectory(Blob blob) {
        return blob.isDirectory() || DIRECTORY_CONTENT_TYPE.equals(blob.getContentType());
    }

    private List<CloudFile> listStorage(boolean includeDirectories) {
        List<CloudFile> files = new ArrayList<>();
        for (Blob blob : getBucketItems().getValues()) {
            if (includeDirectories || !isDirectory(blob)) {
                files.add(createCloudFileFromBlob(blob));
            }
        }

        return files;
    }

    public Blob createBlob(String path, byte[] contents, Map<String, String> metadata) {
        return createBlob(path, contents, metadata, BINARY_CONTENT_TYPE);
    }

    public Blob createBlob(String path, byte[] contents, Map<String, String> metadata, String contentType) {
        BlobInfo blobInfo = BlobInfo.newBuilder(getName(), path).setContentType(contentType).setMetadata(metadata).build();
        return theStorage.create(blobInfo, contents);
    }

    public Blob saveJsonObject(String path, byte[] contents, Map<String, String> metadata) {
        return createBlob(path, contents, metadata, JSON_CONTENT_TYPE);
    }

    public String getUrl() {
        return theEndpointUrl;
    }

    public Blob createBlob(String path, byte[] contents) {
        return createBlob(path, contents, null);
    }

    public CloudFile createImageBlob(String path, byte[] contents) {
        BlobInfo blobInfo = BlobInfo.newBuilder(getName(), path)
                .setCacheControl("public")
                .setContentType(getImageContentType(path)).build();
        return createCloudFileFromBlob(theStorage.create(blobInfo, contents));
    }

    public Blob createBlobFrom(String path, File file) throws IOException {
        return createBlob(path, Files.toByteArray(file));
    }

    public Blob createBlobFrom(String path, File file, String contentType) throws IOException {
        return createBlob(path, Files.toByteArray(file), null, contentType);
    }

    private String getImageContentType(String path) {
        String ext = StringUtils.getExtension(path);
        if (ext == null) {
            return BINARY_CONTENT_TYPE;
        }

        switch (ext.toLowerCase()) {
            case "png":
                return "image/png";
            case "jpg":
            case "jpeg":
                return "image/jpeg";
            case "gif":
                return "image/gif";
            default:
                return BINARY_CONTENT_TYPE;
        }
    }

    public CloudFile getCloudFile(String path) {

        Blob blob = getBlob(path);
        if (blob == null) {
            return null;
        }

        return createCloudFileFromBlob(blob);
    }

    public boolean exists(String path) {
        return (getBlob(path) != null);
    }

    public Blob getBlob(String path) {
        return theStorage.get(BlobId.of(getName(), path));
    }

    public boolean delete(String path) {
        return theStorage.delete(BlobId.of(getName(), path));
    }

    public Subscriber getNotificationSubscriber() {
        return theNotificationSubscriber;
    }

    public void setNotificationSubscriber(Subscriber theNotificationSubscriber) {
        this.theNotificationSubscriber = theNotificationSubscriber;
    }

    private CloudFile createCloudFileFromBlob(Blob blob) {
        try {
            return new CloudFile(new URL(theEndpointUrl + blob.getName()), blob);
        } catch (MalformedURLException ignore) {
            // will not happen
        }
        return null;
    }

    public void deleteBatch(String path) {
        StorageBatch batch = theStorage.batch();
        Page<Blob> bucketItems = getBucketItems(path);
        for (Blob blob : bucketItems.iterateAll()) {
            batch.delete(blob.getBlobId());
        }
        batch.submit();
    }

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
