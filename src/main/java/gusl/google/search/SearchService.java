package gusl.google.search;

import gusl.node.properties.GUSLConfigurable;
import org.jvnet.hk2.annotations.Contract;

/**
 * @author dhudson
 * @since 09/11/2023
 */
@Contract
public interface SearchService extends GUSLConfigurable {

    void search(String searchPhrase);

}
