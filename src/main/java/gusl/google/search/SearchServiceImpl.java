package gusl.google.search;

import com.google.api.client.http.HttpRequest;
import com.google.api.client.http.HttpRequestInitializer;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.customsearch.v1.Customsearch;
import com.google.api.services.customsearch.v1.model.Result;
import com.google.api.services.customsearch.v1.model.Search;
import gusl.core.exceptions.GUSLException;
import gusl.model.nodeconfig.NodeConfig;
import lombok.CustomLog;
import lombok.NoArgsConstructor;
import org.jvnet.hk2.annotations.Service;

/**
 * @author dhudson
 * @since 09/11/2023
 * <p>
 * https://developers.google.com/custom-search/v1/reference/rest/v1/cse/list#response
 *
 * Vertex Search
 * https://cloud.google.com/generative-ai-app-builder/docs/libraries#client-libraries-install-java
 * https://cloud.google.com/generative-ai-app-builder/docs/snippets
 *
 * https://cloud.google.com/discovery-engine/media/docs/movie-rec-tutorial
 */
@CustomLog
@NoArgsConstructor
@Service
public class SearchServiceImpl implements SearchService {

    private static final int HTTP_REQUEST_TIMEOUT = 3 * 600000;

    @Override
    public void search(String searchPhrase) {

        try {
            Customsearch customsearch = new Customsearch(new NetHttpTransport(), new JacksonFactory(), new HttpRequestInitializer() {
                public void initialize(HttpRequest httpRequest) {
                    // set connect and read timeouts
                    httpRequest.setConnectTimeout(HTTP_REQUEST_TIMEOUT);
                    httpRequest.setReadTimeout(HTTP_REQUEST_TIMEOUT);
                }
            });

            Customsearch.Cse.List list = customsearch.cse().list();
            list.setKey("AIzaSyC0a4DfYLiafR5XG8BBwM-1VIw1CEr2ZnQ");
            list.setCx("77cd4f437ed154dba");
            list.setQ(searchPhrase);

            Search results = list.execute();

            for (Result item : results.getItems()) {
                logger.info("item .. {}  ----- {}", item.getHtmlSnippet(), item.getSnippet());
            }


            // resultList = results.getItems();


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void configure(NodeConfig config) throws GUSLException {

    }
}
