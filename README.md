# Assets.

The Assets project handles the assets that are currently stored in the Google CDN.

The 'buckets' are controlled within the Assets project of GCP.

Each bucket as a name, type and optional subscription id.

Image buckets should have full public access as they are used by email and the App.

NB: Buckets are created globally, so be careful when choosing a name.

There is an optional subscribe topic for each storage bucket, any changed will fire a `AssetBucketNotificationEvent`

## Control

https://console.cloud.google.com/storage/browser?project=assets-247007&authuser=1

Project ID assets-247007

Access is controlled via the Service Account casanova-management.

## Buckets

Casanova understands three types of bucket, Image, Documentation and Repository.

Image, is where public images are stored and should have public access.
Documentation, is where player private information is stored, and access to the documentation is granted at a
GSuit level for the Ops.

Repository, is a private storage for Casanova, for example the GeoIP database lives there.

## Create the optional change notification topic channel

```gcloud init``` to point to the Assets project.

To create the topic ...

```gsutil notification create -t kicktheupdate -f json gs://kickthe```

To create the subscription ...

```gcloud pubsub subscriptions create --topic kicktheupdate kickthe-subscriber```

To list the subscriptions ...

```gcloud pubsub subscriptions list```

If there are many listeners for one subscription, then the messages will be load balanced across the subscriptions.
So for each topic, there are many subscribes for each instance.  Remember that you can only have once subscriber per
instance per subscription.

To list the topics ...

```gsutil notification list gs://[BUCKET_NAME]```

## Buckets

| Name | Storage | Access | Type | Topic | Subscribers |
| --- | ---  | --- | --- | ---- | --- |
| kickthe | Multi | Public | Image | kicktheupdate | kickthe-subscriber[prod, staging, demo, local] |
| holeinmy | Multi | Public | Image |  homeinmyupdate | holeinmy-subscriber[prod, staging, demo, local] |
| dropinthe | Coldline | Private | Documentation | dropintheupdate | dropinthe-subscriber[prod, staging, demo, local] |
| dropinthe-demo | Coldline | Private | Documentation | dropinthe-demoupdate | dropinthe-demo-subscriber[prod, staging, demo, local] |
| lm-repo | Regional | Private | Repository | lm-repoupdate | lm-repo-subscriber[prod, staging, demo, local] |


## CORS

To open up the public images to CORS use ..

```gsutil cors set cors.json gs://[BUCKET_NAME]```

To list CORS access use

```gsutil cors get gs://[BUCKET_NAME]```


## Cache Control

```gsutil setmeta -h "Cache-Control:public, max-age=3600" gs://kickthe/assets/**/*.jpg```


## GCloud login ...
gcloud auth login


